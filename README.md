# Simply Navigation
This is a dead simple lightweight navigation built with flexbox and vanilla JavaScript

Try it here [https://obscuredetour.github.io/simply-nav/](https://obscuredetour.github.io/simply-nav/)

## The gist

- No dependencies. Built using Flexbox & vanilla JavaScript only.
- Fully responsive.
- Easy close mobile menu. Mobile menu closable via entire top row and right side page overlay.
- Mobile menu closes when selecting a link. Useful so the mobile user doesn't have to close the menu after selecting an anchor link on the current page. Try by opening the [mobile menu](https://codepen.io/obscuredetour/full/XxNWLY/) and click on the 'Last Section'.


### Customizable

- Breakpoint variable - default at 800px.
- Sticky navigation bar available

### How to use
- Demo it on [GitHub](https://obscuredetour.github.io/simply-nav/)
- Play with it on [Codepen](https://codepen.io/obscuredetour/full/XxNWLY/)


Clone or download the repo to build a static website. Or alternatively insert the respective files into your project detailed below.

`standalone_ver.html` has all necessary code within. Alternatively `nav.js` & `nav.css` files and a note of how the respective markup is structured within the `index.html` file and you're set.

Sass files provide for best customizability.